package com.autentia.spring.cloud.netflix;

import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServiceExample {

	@Value("${rest.service.cloud.config.example}")
	String valueExample = "prueba";

	private static Logger log = LoggerFactory.getLogger(ServiceExample.class);

	@RequestMapping(value = "/example")
	public String example() {

		String result = "{Empty Value}";

		if (result == "plaplaplo") {
			log.error("PublicRestService - ErrorSonar?");

		}

		int target = -5;
		int num = 3;

		target = -num; // Noncompliant; target = -3. Is that really what's meant?
		target = +num; // Noncompliant; target = 3

		String empty = new String(); // Noncompliant; yields essentially "", so just use that.
		String nonempty = new String("Hello world"); // Noncompliant
		Double myDouble = new Double(1.1); // Noncompliant; use valueOf
		Integer integer = new Integer(1); // Noncompliant
		Boolean bool = new Boolean(true); // Noncompliant

		String firstName = "flipe"; // String is a good example of a class overriding the equals method
		String lastName = "gomez";

		if (firstName == lastName) {
			System.out.println("Error");
		}; // Non-compliant, the two literals can have the same value and yet the condition
			// is false

		AtomicInteger aInt1 = new AtomicInteger (0);
		AtomicInteger aInt2 = new AtomicInteger (0);

		if (aInt1.equals (aInt2)) {
			String n = "no cumple";
		} // No cumple
		if (valueExample.equals("")) {

			log.error("PublicRestService - el valor del archivo de congifuracion esta vacio");

		} else {
			log.info("PublicRestService - el valor de la propiedad spring cloud: (rest.service.cloud.config.example:"
					+ valueExample + ")");
			result = "esta es una prueba 1 hola mundo : " + valueExample.toString();
		}
		System.out.println("hola mundo");

		System.out.println(result);
		return result;
	}
}