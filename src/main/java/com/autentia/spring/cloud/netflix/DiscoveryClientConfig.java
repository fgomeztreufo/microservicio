package com.autentia.spring.cloud.netflix;

import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

//@Profile("!local")
@Profile("local")
@Configuration
@EnableDiscoveryClient
public class DiscoveryClientConfig {

}
