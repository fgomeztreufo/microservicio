package com.autentia.spring.cloud.netflix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableDiscoveryClient
@SpringBootApplication
@EnableEurekaClient
public class ApplicationRestService extends SpringBootServletInitializer{
	
//  public static void main(String[] args) {
//    SpringApplication.run(ApplicationRestService.class, args);
//  }
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ApplicationRestService.class);
	}
	
	public static void main(String[] args) {
		SpringApplication.run(ApplicationRestService.class, args);
	}
}